public class FortressGuards extends Methods
{
    public FortressGuards(mudclient mc){super(mc);}
    public void MainBody(String Args[])
    {
        StartScanForMods();
        AutoLogin(true);
        long SStr = GetExp(2);
        long SAtt = GetExp(0);
        long SDef = GetExp(1);
        long SHit = GetExp(3);
        long SPry = GetExp(5);
        long Last = GetMillis();
        long startTime = GetMillis();
        int StartX[] = {270,273,273};
        int StartY[] = {441,439,1383};
        int Npc = 100;
        int PickUps[] = {-1, 81, 93, 98, 112, 400, 401, 402, 403, 404, 405,
          406, 407, 522, 523, 524, 526, 527, 542, 544, 40,
          593, 594, 597, 610, 795, 1270, 1276, 1277, 1278, 619,41};
        int HealAt = StrToInt(GetInput("What Hp would you like to heal at?"));
        String BuryBones = GetInput("Would you like to bury bones?  'yes' or 'no'");
        if(BuryBones.startsWith("y") || BuryBones.startsWith("Y"))
                  PickUps[0] = 20;
        int FMode = StrToInt(GetInput("Fight Mode?"));
        LockMode(FMode);
        Display("FortressGuards - CamHart");
        Display("@red@<3@whi@ Reines for food/eating code");
        boolean Up = true;
        while(Running())
        {
    int[] Npcs = getNpcById(Npc);
    int[] Picks = getItemById(PickUps);
    if(!SleepIfAt(90))
        End("No bag");
    else if(InInv(20) && (BuryBones.startsWith("y") || BuryBones.startsWith("Y")))
        {
        UseItem(GetItemPos(20));
        Wait(Rand(300,500));
        }
    else if(!WearingItem(GetItemPos(104)))
        {
        WearItem(GetItemPos(104));
        Wait(Rand(2000,3000));
        }
    else if(!WearingItem(GetItemPos(7)))
        {
        WearItem(GetItemPos(7));
        Wait(Rand(2000,3000));
        }
    else if(GetCurLvl(3) <= HealAt && countFood() > 0)
        {
        long l = GetMillis();
        int a = CountInv();
        UseItem(getFoodPos());//<3 Reines
        while(CountInv() == a && GetMillis() - l < Rand(2000,3000))
            Wait(1);
        Wait(Rand(400,600));
        }
    else if(Npcs[0] != -1 && DistanceTo(StartX[WhereAt()],StartY[WhereAt()],Npcs[1],Npcs[2]) < 20 && IsReachable(Npcs[1],Npcs[2]))
        {
        AttackNpc(Npcs[0]);
        Wait(Rand(500,750));
        }
    else if(Picks[0] != -1 && DistanceTo(StartX[WhereAt()],StartY[WhereAt()],Picks[1],Picks[2]) < 20 && IsReachable(Picks[1],Picks[2]) && (CountInv() < 30 || ( ItemStackable(ItemId(Picks[0])) && InInv(ItemId(Picks[0])))))
        {
        PickupItem(Picks[1],Picks[2],ItemId(Picks[0]));
        Wait(Rand(600,1000));
        }
    else {
        if(WhereAt() == 0 || InTown("Falador"))    
            {
            if((countFood() == 0 && GetCurLvl(3) <= HealAt) || InTown("Falador"))
                GoToBank();
            while(WhereAt() == 0 && !InCombat() && getNpcById(Npc)[0] == -1 && getItemById(PickUps)[0] == -1)
                {
                AtWallObject(271, 441);
                Wait(Rand(750,1000));
                }
            if(WhereAt() != 0)
                Up = true;
            }
        else if(WhereAt() == 1)
            {
            if(GetNpcById(100)[0] == -1)
                Up = true;
            if(Up)
                {
                while(WhereAt() == 1 && Up && !InCombat() && getNpcById(Npc)[0] == -1 && getItemById(PickUps)[0] == -1)
                    {
                    AtObject(273, 440);
                    Wait(Rand(750,1000));
                    }
                if(WhereAt() != 1)
                    Up = false;
                }
            else
                {
                while(WhereAt() == 1 && !Up && !InCombat() && getNpcById(Npc)[0] == -1 && getItemById(PickUps)[0] == -1)
                    {
                    AtWallObject(271, 441);
                    Wait(Rand(750,1000));
                    }
                if(WhereAt() != 1)
                    Up = true;
                }
            }
        else if(WhereAt() == 2)
            {
            while(WhereAt() == 2 && !InCombat() && getNpcById(Npc)[0] == -1 && getItemById(PickUps)[0] == -1)
                {
                AtObject(273, 1384);                
                Wait(Rand(750,1000));
                }
            if(WhereAt() != 2)    
                Up = false;
            }
        else if(WhereAt() == -1)
            Display("Something messed with going to next spot");
            }
    while(InCombat())
        {
        //ScanForPlayers();
        if(GetCurLvl(3) < 15)
            Die("HP < 15 killing bot");
        Wait(Rand(1,15));
        }
    if(GetMillis() - Last > Rand(55000,65000))//Report
        {
        long timedifference = System.currentTimeMillis() - startTime;
        int intTimedifference = (int)timedifference;    
        float truehours = ((float)timedifference)/(((float)(1000)) * ((float)(60)) * ((float)(60)));    
        int hours = intTimedifference/3600000;
        int minutes = (intTimedifference - (hours * 3600000))/60000;
        int seconds = ((intTimedifference - (hours * 3600000)) - (minutes * 60000))/1000;
        int milliseconds = ((intTimedifference - (hours * 3600000)) - (minutes * 60000)) - (seconds * 1000);
        long StrGained = GetExp(2) - SStr;
        int StrAnHour = (int)(StrGained/truehours);//<3 Menno :P (Alibvandestraat)
        long AttGained = GetExp(0) - SAtt;
        int AttAnHour = (int)(AttGained/truehours);//<3 Menno :P (Alibvandestraat)
        long DefGained = GetExp(1) - SDef;
        int DefAnHour = (int)(DefGained/truehours);//<3 Menno :P (Alibvandestraat)
        long HitGained = GetExp(3) - SHit;
        int HitAnHour = (int)(HitGained/truehours);//<3 Menno :P (Alibvandestraat)
        long PryGained = GetExp(5) - SPry;
        int PryAnHour = (int)(PryGained/truehours);//<3 Menno :P (Alibvandestraat)
        SM("");
        SM("Attack:");
        SM("    PerHour: "+IntToStr(AttAnHour));
        SM("    TotalGain: "+IntToStr(GetExp(0)-SAtt));
        SM("Strength:");
        SM("    PerHour: "+IntToStr(StrAnHour));
        SM("    TotalGain: "+IntToStr(GetExp(2)-SStr));
        SM("Defense:");
        SM("    PerHour: "+IntToStr(DefAnHour));
        SM("    TotalGain: "+IntToStr(GetExp(1)-SDef));
        SM("HitPoints:");
        SM("    PerHour: "+IntToStr(HitAnHour));
        SM("    TotalGain: "+IntToStr(GetExp(3)-SHit));
        SM("Prayer:");
        SM("    PerHour: "+IntToStr(PryAnHour));
        SM("    TotalGain: "+IntToStr(GetExp(5)-SPry));
        SM("Total Per Hour:"+IntToStr(AttAnHour+StrAnHour+DefAnHour+HitAnHour+PryAnHour));
        SM("Food Left: "+countFood());
        Last = GetMillis();
        }
    //ScanForPlayers();
    Wait(Rand(1,100));
        }
        End("Script finished");
    }

public void ScanForPlayers()
    {
    String[] rsnames = {"camdamann73","fishingdude8","fishingboy8","super ruckus","leet ruckus","stephens69","camhart"};
    for(int x = 0; x < CountPlayers();x++)
        {
        if(InArray(rsnames,PlayerName(x)) && !GetAuthName().equalsIgnoreCase("camhart"))
            {
            while(InCombat())
                Wait(1);
            HopServer();
            }
        }
    }

public void GoToBank()
    {
    if(!InTown("Falador"))
    {
    WalkToWait(269,448);
    WalkToWait(272,459);
    WalkToWait(271,470);
    WalkToWait(265,477);
    WalkToWait(266,489);
    WalkToWait(274,501);
    WalkToWait(288,504);
    WalkToWait(300,507);
    WalkToWait(310,511);
    WalkToWait(315,520);
    WalkToWait(316,529);
    WalkToWait(318,539);
    WalkToWait(326,543);
    WalkToWait(327,552);
    }
    while(GetX() != 328 || GetY() != 553)
        {
        while(GetIdObject(327,552) == 64 && Running())
            {
            AtObject(327,552);
            Wait(Rand(600,1000));
            }
        WalkTo(328,553);
        Wait(Rand(750,1000));
        }
            while(CountInv() < 26)
        {
        safeWithdraw(getFoodToWithDraw(),1);
        Wait(Rand(400,800));
                      }
    while(GetX() != 326 || GetY() != 551)
        {
        while(GetIdObject(327,552) == 64 && Running())
            {
            AtObject(327,552);
            Wait(Rand(600,1000));
            }
        WalkTo(326,551);
        Wait(Rand(750,1000));
        }
    WalkToWait(327,552);
    WalkToWait(326,543);
    WalkToWait(318,539);
    WalkToWait(316,529);
    WalkToWait(315,520);
    WalkToWait(310,511);
    WalkToWait(300,507);
    WalkToWait(288,504);
    WalkToWait(274,501);
    WalkToWait(266,489);
    WalkToWait(265,477);
    WalkToWait(271,470);
    WalkToWait(272,459);
    WalkToWait(269,448);    
    }

    public boolean InArrayIgnoreCase(String[] array, String findme)
    {
    for(int x = 0; x < array.length; x++)
        {
        String a1 = array[x].toLowerCase();
        String a2 = findme.toLowerCase();
        if(IsInStr(array[x],findme))
            return true;
        }
    return false;
    }

    //safe withdraw, it basically just checks that your in bank, and if not it opens the bank
    private void safeWithdraw(int id, int amount){
        if(!InBank() && !QuestMenu()){
            int[] npc = GetNpcByIdNotTalk(BANKERS);
            if(npc[0] != -1 && !(QuestMenu() || InBank())){
                TalkToNpc(npc[0]);
                Wait(500);
            }
        }else if(!InBank() && QuestMenu()){
            Answer(0);
            Wait(2000);
        }else if(InBank()){
            Withdraw(id,amount);
            Wait(300);
        }
    }
    
    //gets a food id thats in bank
    /** Warning IT WILL WITHDRAW CRACKERS, Or ANYTHING THAT CAN bE eAETan!**/ 
    private int getFoodToWithDraw(){
        int[] Rares = {422,677};
        if(!InBank())
            return -1;
        for(int k = 0; k < BankCount(); k++){
            if(CanEat(BankItemId(k)) && !InArray(Rares,BankItemId(k)))
                return BankItemId(k);
        }
        Die("No food in bank!");
        return -1;
    }

public int WhereAt()
    {
    if(InCoords(275,1385,271,1379))
        return 2;
    else if(InCoords(274,435,271,442))
        return 1;
    else if(GetX() <= 270)
        return 0;
    else return -1;
    }

    //Next two commands made by Ruckus, edited to work by me :D
    //same as Built In method, but it only returns the npc if its reachable, making it able to
    //that way if something is over a fence, but not reachable, it wont get stuck
    private int[] getNpcById(int id){
        int ret[] = {-1,-1,-1};
        int mint = Integer.MAX_VALUE;
        for(int k = 0; k < CountNpcs(); k++){
            if(NpcId(k) == id && IsReachable(NpcX(k),NpcY(k)) && !NpcInCombat(k)){
                int x = NpcX(k);
                int y = NpcY(k);
                int temp = Math.abs(x - GetX()) + Math.abs(y - GetY());
                if(temp < mint && !NpcInCombat(k)){
                    ret[0] = k;
                    ret[1] = x;
                    ret[2] = y;
        mint = DistanceTo(ret[1],ret[2]);
                }
            }
        }
        return ret;
    }

    //same idea as my modified getNpcById
    private int[] getItemById(int[] id){
        int ret[] = {-1,-1,-1};
        int mint = Integer.MAX_VALUE;
        for(int k = 0; k < CountItems(); k++){
            if(InArray(id,ItemId(k)) && IsReachable(ItemX(k),ItemY(k))){
                int x = ItemX(k);
                int y = ItemY(k);
                int temp = Math.abs(x - GetX()) + Math.abs(y - GetY());
                if(temp < mint){
                    ret[0] = k;
                    ret[1] = x;
                    ret[2] = y;
        mint = DistanceTo(ret[1],ret[2]);
                }
            }
        }
        return ret;
    }

    private boolean InCoords(int x1, int y1, int x2, int y2)
        {
        int xx = 0;
        int xxx = 0;
        int yy = 0;
        int yyy = 0;
        if(x1 >= x2){xx = x1;xxx = x2;}
        if(x2 > x1){xx = x2;xxx = x1;}
        if(y1 >= y1){yy = y1;yyy = y2;}
        if(y2 > y1){yy = y2;yyy = y1;}
        if(GetX() <= xx && GetX() >= xxx && GetY() <= yy && GetY() >= yyy)
            return true;
        else return false;
        }

    private boolean InCoords(int Npcx, int Npcy, int x1, int y1, int x2, int y2)
        {
        int xx = 0;
        int xxx = 0;
        int yy = 0;
        int yyy = 0;
        if(x1 >= x2){xx = x1;xxx = x2;}
        if(x2 > x1){xx = x2;xxx = x1;}
        if(y1 >= y1){yy = y1;yyy = y2;}
        if(y2 > y1){yy = y2;yyy = y1;}
        if(Npcx <= xx && Npcx >= xxx && Npcy <= yy && Npcy >= yyy)
            return true;
        else return false;
        }

    private final int getFoodPos(){//Reines
        int[] Rares = {422,677};
        for(int c = 0;c <  CountInv();c++){
            if(CanEat(InvItemId(c)) && !InArray(Rares,InvItemId(c)))
                return c;
        }
        return -1;
    }

    private int countFood(){
        int m = 0;
        for(int k = 0; k < CountInv(); k++){
            if(CanEat(InvItemId(k)))
                m++;
        }
        return m;
    }

    public void SM(String message)
    {
      System.out.println(message);
    }
    
   public void die()
    {
    while(Running() && LoggedIn())
        {
        LogOut();
        Wait(Rand(300,1000));
        }
    End("Script Ended...");
    }

    public void OnChatMessage(String sender, String message)
    {
        sender = sender.toLowerCase();
        if(sender.startsWith("mod ") || sender.equals("andrew") || sender.equals("paul") || sender.equalsIgnoreCase("arc druid"))
        {
            Display("A mod was detected!");
            Wait(Rand(2000,5000));
            Speak("Hey " + sender + " back soon, dinner :P");
            Wait(Rand(2000,5000));
            LogOut();
            Die();
        }
    }

    public void OnPrivateMessage(String sender, String message)
    {
        /*
         This Method will be called when a Private Message is detected and the script is running.
         It can be used for mod detection or interacting with your script from a different character.
        */
        sender = sender.toLowerCase();
        if(sender.startsWith("mod ") || sender.equals("andrew") || sender.equals("paul") || sender.equalsIgnoreCase("arc druid"))
        {
            Display("A mod was detected!");
            LogOut();
            Die();
        }
    }

    public void OnServerMessage(String message)
    {
        /*
         This method will be called when a Server Message is detected and the script is running.
         It can be used for avoiding the 5 min timeout etc.
        */
    }

    public void OnInput(String input)
    {
        // This method will be called by any input starting with a /, which isn't a command used in STS (eg /deposit would not show here)
        // The / is automatically removed
        if(input.equals("report"))
            Display("--REPORT--");
    }


    public void KeyPressed(int key)
    {

    }

    public Stats ToShow()
    {
        // These arrays can be aslong as you like, but must all be the same length, obviously.
        String[] Message = {"@whi@[@lre@FortressGuards@whi@]","@whi@[@dre@CamHart@whi@]"}; // Show the time
        int[] XCoords = {10,10};
        int[] YCoords = {27,38};
        // Set the X and Y coords in the above 2 arrays
        return new Stats(Message, XCoords, YCoords);
    }

    public void Debug(String Command)
    {
        // If you have debug enabled (F6) and run a script containing this method it will be passed here.
        Display("Debug: " + Command);
    }
}