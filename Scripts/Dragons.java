public class Dragons extends Methods {
    public Dragons(mudclient mc){super(mc);}

    /*
    Inv:
Rune Large
Rune Chain
Rune Kite
Klanks
Rune Legs
Drag Ammy
Drag Long
Sleeping Bag
Cakes (20)
Dusty Key
Anti Dragon Breath
~1k dragon bones a day*/

    public int[] CollectIDs = {814,523,526,527,795};
    public int[] ItemCounter = {0,0,0,0,0};
    public int NPC = 202;
    public boolean bspot1 = false;
    public boolean bspot2 = false;
    public boolean bspot3 = false;
    public boolean bspot4 = false;
    public boolean break1 = false;
    public int[] pathx1 = {342,342,342,342,342,342,342,342,342};
    public int[] pathy1 = {3367,3368,3369,3370,3371,3372,3373,3374,3375};
    public int[] pathx2 = {342,342,342,342,342,342,342,342,342};;
    public int[] pathy2 = {3375,3374,3373,3372,3371,3370,3369,3368,3367};
    long CB = -1;
    public boolean fighting = false;
    
    public int TestForNPC(int x,int y)
    {
        int id = -1;
        for(int i = 0; i < CountNpcs(); i++)
        {
            if(NpcX(i) == x && NpcY(i) == y)
                id = i;
        }
        return id;
    }
    
    public int TestForPlayer(int x,int y)
    {
        int id = -1;
        for(int i = 0; i < CountPlayers(); i++)
        {
            if(PlayerX(i) == x && PlayerY(i) == y)
                id = i;
        }
        return id;
    }
    
    public void WalkPath(int[] x,int[] y)
    {
        long TO0 = System.currentTimeMillis();
        for(int i = 0; i < x.length; i++)
        {
            while((GetX() != x[i] || GetY() != y[i]) && System.currentTimeMillis() - TO0 < Rand(30000,40000))
            {
                if(TestForNPC(x[i],y[i]) == -1)
                {
                    WalkTo(x[i],y[i]);
                    long l1 = System.currentTimeMillis();
                    while(Running() && System.currentTimeMillis() - l1 < Rand(1025,1365) && (GetX() != x[i] || GetY() != y[i]))
                        Wait(Rand(10,20));
                }
                else
                {
                    int npc = TestForNPC(x[i],y[i]);
                    if(npc != -1)
                        AttackNpc(npc);
                    long l1 = System.currentTimeMillis();
                    while(Running() && System.currentTimeMillis() - l1 < Rand(1025,1365) && (GetX() != x[i] || GetY() != y[i]))
                        Wait(Rand(10,20));
                }
                if(TestForPlayer(x[i],y[i]) == -1)
                {
                    if(GetX() != x[i] || GetY() != y[i])
                        WalkTo(x[i],y[i]);
                    long l1 = System.currentTimeMillis();
                    while(Running() && System.currentTimeMillis() - l1 < Rand(1025,1365) && (GetX() != x[i] || GetY() != y[i]))
                        Wait(Rand(10,20));
                }
                else
                {
                    if(GetX() != x[i] || GetY() != y[i])
                        ForceWalkTo(x[i],y[i]);
                    long l1 = System.currentTimeMillis();
                    while(Running() && System.currentTimeMillis() - l1 < Rand(1025,1365) && (GetX() != x[i] || GetY() != y[i]))
                        Wait(Rand(10,20));
                }
            }
        }
    }
    
    public void Walk(int x1, int y1)
    {
        WalkToWait(x1 + Rand(0,2),y1 + Rand(0,2));
    }
    
    /*public void Walk(int x1,int y1)
    {
        int x2 = x1 + 1;
        int y2 = y1 + 1;
        while((GetX() != x1 && GetX() != x2) || (GetY() != y1 && GetY() != y2))
        {
            int x = -1;
            int y = -1;
            int[] spot1 = {TestForNPC(x1,y1),TestForPlayer(x1,y1)};
            int[] spot2 = {TestForNPC(x1,y2),TestForPlayer(x1,y2)};
            int[] spot3 = {TestForNPC(x2,y1),TestForPlayer(x2,y1)};
            int[] spot4 = {TestForNPC(x2,y2),TestForPlayer(x2,y2)};
            if(spot1[0] == -1 && spot1[1] == -1)
                bspot1 = true;
            if(spot2[0] == -1 && spot2[1] == -1)
                bspot2 = true;
            if(spot3[0] == -1 && spot3[1] == -1)
                bspot3 = true;
            if(spot4[0] == -1 && spot4[1] == -1)
                bspot4 = true;
            while(x == -1 && y == -1)
            {
                int randomizer = Rand(0,4);
                if(randomizer == 0 && bspot1)
                {
                    x = x1;
                    y = y1;
                }
                if(randomizer == 1 && bspot2)
                {
                    x = x1;
                    y = y2;
                }
                if(randomizer == 2 && bspot3)
                {
                    x = x2;
                    y = y1;
                }
                if(randomizer == 3 && bspot4)
                {
                    x = x2;
                    y = y2;
                }
                if(!bspot1 && !bspot2 && !bspot3 && !bspot4)
                {
                    x = x1;
                    y = y1;
                }
            }
            if(TestForPlayer(x,y) == -1)
            {
                if(GetX() != x || GetY() != y)
                    WalkTo(x,y);
                long l1 = System.currentTimeMillis();
                while(Running() && System.currentTimeMillis() - l1 < Rand(2987,3364) && (GetX() != x1 && GetX() != x2) || (GetY() != y1 && GetY() != y2))
                    Wait(Rand(10,20));
            }
            else
            {
                if(GetX() != x || GetY() != y)
                    ForceWalkTo(x,y);
                long l1 = System.currentTimeMillis();
                while(Running() && System.currentTimeMillis() - l1 < Rand(2987,3364) && (GetX() != x1 && GetX() != x2) || (GetY() != y1 && GetY() != y2))
                    Wait(Rand(10,20));
            }

        }

    } */
    
    public void CheckSleep()
    {
        while((GetFatigue() > 95 || Sleeping()) && Running())
        {
            while(!Sleeping() && Running())
            {
                System.out.println("Sleeping...");
                UseItem(GetItemPos(1263));
                Wait(Rand(1000,2000));
            }
            while(Sleeping() && Running())
                Wait(Rand(1000,1500));
        }
    }
    
    public void CheckSleep2()
    {
        while((GetFatigue() > 1 || Sleeping()) && Running())
        {
            while(!Sleeping() && Running())
            {
                System.out.println("Sleeping...");
                UseItem(GetItemPos(1263));
                Wait(Rand(1000,2000));
            }
            while(Sleeping() && Running())
                Wait(Rand(1000,1500));
        }
    }
    
    public void EatACake()
    {
        if(CountInv(330) != 0)
        {
            int startcount = CountInv(330);
            int startcount1 = CountInv(333);
            int startcount2 = CountInv(335);
            long loop = System.currentTimeMillis();
            while(CountInv(330) == startcount && Running() && System.currentTimeMillis() - loop < 60000)
            {
                System.out.println("eating first piece");
                UseItem(GetItemPos(330));
                long l1 = System.currentTimeMillis();
                while(Running() && System.currentTimeMillis() - l1 < Rand(6000,9000) && CountInv(330) == startcount && !InCombat())
                    Wait(Rand(10,20));
                while(InCombat() && System.currentTimeMillis() - loop < 60000)
                {
                    System.out.println("in combat 1");
                    Wait(Rand(10,20));
                    if((GetMaxLvl(3) - GetCurLvl(3)) > 40)
                    {
                        WalkTo(GetX(),GetY());
                        long TO = System.currentTimeMillis();
                        while(Running() && System.currentTimeMillis() - TO < Rand(8000,12000) && InCombat())
                            Wait(Rand(40,60));
                    }
                }
            }
            long TO1 = System.currentTimeMillis();
            while(CountInv(333) == startcount1 && System.currentTimeMillis() - TO1 < Rand(6000,9000))
                Wait(1);
            while(CountInv(333) > 0 && Running() && System.currentTimeMillis() - loop < 60000)
            {
                int count1 = CountInv(333);
                UseItem(GetItemPos(333));
                long l1 = System.currentTimeMillis();
                while(Running() && System.currentTimeMillis() - l1 < Rand(6000,9000) && CountInv(333) == count1 && !InCombat())
                    Wait(Rand(10,20));
                while(InCombat() && System.currentTimeMillis() - loop < 60000)
                {
                    System.out.println("in combat 1");
                    Wait(Rand(10,20));
                    if((GetMaxLvl(3) - GetCurLvl(3)) > 40)
                    {
                        WalkTo(GetX(),GetY());
                        long TO = System.currentTimeMillis();
                        while(Running() && System.currentTimeMillis() - TO < Rand(8000,12000) && InCombat())
                            Wait(Rand(40,60));
                    }
                }
            }
            long TO2 = System.currentTimeMillis();
            while(CountInv(335) == startcount2 && System.currentTimeMillis() - TO2 < Rand(6000,9000))
                Wait(1);
            while(CountInv(335) > 0 && Running() && System.currentTimeMillis() - loop < 60000)
            {
                int count2 = CountInv(335);
                UseItem(GetItemPos(335));
                long l1 = System.currentTimeMillis();
                while(Running() && System.currentTimeMillis() - l1 < Rand(6000,9000) && CountInv(335) == count2 && !InCombat())
                    Wait(Rand(10,20));
                while(InCombat() && System.currentTimeMillis() - loop < 60000)
                {
                    System.out.println("in combat 1");
                    Wait(Rand(10,20));
                    if((GetMaxLvl(3) - GetCurLvl(3)) > 40)
                    {
                        WalkTo(GetX(),GetY());
                        long TO = System.currentTimeMillis();
                        while(Running() && System.currentTimeMillis() - TO < Rand(8000,12000) && InCombat())
                            Wait(Rand(40,60));
                    }
                }
            }
        }
    }
    
    public void Monitor()
    {
        new Thread
        (
            new Runnable()
            {
                public void run()
                {
                    while (Running())
                    {
                    if((GetMaxLvl(3) - GetCurLvl(3)) > 60)
                        Die();
                    }
                }
            }
        ).start();
    }
    
    public void MainBody(String Args[])
    {
        System.out.println("Dragon Killer - by kingokings: Started");
        StartScanForMods();
        AutoLogin(true);
        LockMode(1);
        Monitor();
        long exp = GetExp(1);
        long starttime = System.currentTimeMillis();
        while(Running())
        {
           //if inside dungeon - start at dragons
           if(GetY() > 1000)
           {
               if(GetX() > 373)
               {
                   //walk to lair
                   Walk(376,3341);
                   Walk(376,3333);
                   Walk(374,3323);
                   Walk(366,3320);
                   Walk(359,3322);
                   Walk(354,3329);
                   Walk(349,3334);
                   Walk(345,3343);
                   Walk(341,3351);
                   Walk(337,3360);
                   Walk(330,3370);
                   Walk(337,3366);
                   Walk(342,3365);
                   WalkPath(pathx1,pathy1);
                   Walk(351,3375);
                   Walk(348,3369);
                   Walk(349,3362);
                   while(GetX() < 352 && Running())
                   {
                       WalkTo(354,3353);
                       Wait(Rand(1000,1200));
                   }
                   //go through door
                   while(GetX() < 355 && Running())
                   {
                       while(InCombat())
                       {
                           WalkTo(354,3353);
                           long l3 = System.currentTimeMillis();
                           while(InCombat() && System.currentTimeMillis() - l3 <= Rand(800,1200))
                               Wait(Rand(100,120));
                       }
                       if(!InCombat())
                       {
                           UseOnWallObject(GetItemPos(596),355,3353);
                           Wait(Rand(800,1200));
                       }
                   }
                   if(!WearingItem(GetItemPos(420)))
                   {
                       WearItem(GetItemPos(420));
                       Wait(Rand(800,1200));
                   }
                   //walk to spot
                   while(GetX() < 365 && Running())
                   {
                       WalkToWait(369,3353);
                   }
               }

               //fight dragons
               System.out.println("Starting to fight dragons!");
               while(Running() && (CountInv(330) > 0 || CountInv(333) > 0 || CountInv(335) > 0))
               {
                   fighting = true;
                   if(CountInv() == 30 || ((GetMaxLvl(3) - GetCurLvl(3)) > 12 && !InCombat()) || (!InCombat() && !WearingItem(GetItemPos(420))))
                   {
                       if(CountInv() == 30 || (GetMaxLvl(3) - GetCurLvl(3)) > 12)
                       {
                           System.out.println("Eating a cake!");
                           EatACake();
                       }
                       if(!InCombat() && !WearingItem(GetItemPos(420)))
                       {
                           WearItem(GetItemPos(420));
                           Wait(Rand(800,1200));
                       }
                   }
                   else
                   {
                       while((GetFatigue() > 95 || Sleeping()) && Running())
                       {
                           while(!Sleeping() && Running())
                           {
                               System.out.println("Sleeping...");
                               UseItem(GetItemPos(1263));
                               Wait(Rand(1000,2000));
                           }
                           while(Sleeping() && Running())
                               Wait(Rand(1000,1500));
                       }
                       if(!InCombat())
                       {
                           int[] nearestitem = GetItemById(CollectIDs);
                           if(nearestitem[0] != -1 && IsReachable(nearestitem[1],nearestitem[2]))
                           {
                               Wait(Rand(40,60));
                               int invcount = CountInv();
                               PickupItemById(CollectIDs);
                               long TO = System.currentTimeMillis();
                               while(Running() && System.currentTimeMillis() - TO < Rand(2000,3000) && invcount == CountInv() && !InCombat())
                                   Wait(Rand(40,60));
                           }
                           else
                           {
                               int[] Target = GetNpcById(NPC);
                               if(Target[0] != -1 && !InCombat() && IsReachable(Target[1],Target[2]))
                               {
                                   AttackNpc(Target[0]);
                                   Wait(Rand(500,700));
                               }
                           }
                       }
                       if(InCombat())
                       {
                           if(CB == -1)
                               CB = System.currentTimeMillis();
                           if(System.currentTimeMillis() - CB > Rand(600,800))
                           {
                               if(!WearingItem(GetItemPos(404)))
                               {
                                   WearItem(GetItemPos(404));
                                   Wait(Rand(800,1200));
                               }
                               CB = -1;
                           }
                           if((GetMaxLvl(3) - GetCurLvl(3)) > 40)
                           {
                               WalkTo(GetX(),GetY());
                               long TO = System.currentTimeMillis();
                               while(Running() && System.currentTimeMillis() - TO < Rand(8000,12000) && InCombat())
                                   Wait(Rand(40,60));
                           }
                           Wait(1);
                       }
                   }
               }
               System.out.println("Done fighting dragons!");
               fighting = false;
               if(!WearingItem(GetItemPos(420)))
               {
                   WearItem(GetItemPos(420));
                   Wait(Rand(800,1200));
               }

               int[] nearestitem = GetItemById(CollectIDs);
               while(nearestitem[0] != -1 && IsReachable(nearestitem[1],nearestitem[2]) && (GetMaxLvl(3) - GetCurLvl(3)) < 20)
               {
                   Wait(Rand(40,60));
                   int invcount = CountInv();
                   PickupItemById(CollectIDs);
                   long TO = System.currentTimeMillis();
                   while(Running() && System.currentTimeMillis() - TO < Rand(2000,3000) && invcount == CountInv() && !InCombat())
                       Wait(Rand(40,60));
                   nearestitem = GetItemById(CollectIDs);
               }
               
               //walk to door
               while(GetX() > 356 && Running())
               {
                   WalkTo(355,3353);
                   Wait(Rand(1000,1200));
               }
               //go through door
               while(GetX() > 354 && Running())
               {
                   if(InCombat())
                   {
                       WalkTo(GetX(),GetY());
                       long l3 = System.currentTimeMillis();
                       while(InCombat() && System.currentTimeMillis() - l3 <= Rand(800,1200))
                           Wait(Rand(100,120));
                   }
                   if(!InCombat())
                   {
                       UseOnWallObject(GetItemPos(596),355,3353);
                       Wait(Rand(800,1200));
                   }
               }
               
               if(!WearingItem(GetItemPos(404)))
               {
                   WearItem(GetItemPos(404));
                   Wait(Rand(800,1200));
               }

               //walk to ladder
               Walk(349,3362);
               Walk(348,3369);
               Walk(351,3375);
               WalkPath(pathx2,pathy2);
               Walk(342,3365);
               Walk(337,3366);
               Walk(330,3370);
               Walk(337,3360);
               Walk(341,3351);
               Walk(345,3343);
               Walk(349,3334);
               Walk(354,3329);
               Walk(359,3322);
               Walk(366,3320);
               Walk(374,3323);
               Walk(376,3333);
               Walk(376,3341);
               Walk(376,3351);
               //go up ladder
               while(GetY() > 1000)
               {
                   AtObject(376,3352);
                   Wait(Rand(1400,1700));
               }


           }
           
           //if outside dungeon - start at bank
           if(GetY() < 1000)
           {
               //if coming from dungeon
               if(GetX() > 338)
               {
                   Walk(371,528);
                   Walk(362,536);
                   Walk(354,543);
                   Walk(345,549);
                   Walk(340,554);
                   CheckSleep2();
                   while(Running() && GetX() != 338)
                   {
                       AtObject(339,555);
                       Wait(Rand(800,1200));
                   }
               }
               
               //if at bank
               if(GetX() < 339)
               {
                   Walk(333,558);
                   Walk(325,553);
                   //check door
                   while(GetX() < 328 && Running())
                   {
                       if(GetIdObject(327,552) == 64)
                       {
                           AtObject(327,552);
                           Wait(Rand(800,1200));
                       }
                       WalkTo(329 + Rand(0,2),552 + Rand(0,2));
                       Wait(Rand(800,1200));
                   }
                   //record amounts
                   for(int i = 0; i < CollectIDs.length; i++)
                   {
                       if(CountInv(CollectIDs[i]) > 0 && Running())
                       ItemCounter[i] = (ItemCounter[i] + CountInv(CollectIDs[i]));
                   }
                   //bank if needed
                   System.out.println("Banking...");
                   while(Running() && CountInv(CollectIDs) > 0)
                   {
                       while(!InBank())
                       {
                           while(!QuestMenu())
                           {
                               int[] banker = GetNpcById(95);
                                         if (banker[0] != -1)
                                         {
                                   TalkToNpc(banker[0]);
                                   Wait(Rand(1500,1600));
                               }
                           }
                           Wait(Rand(100,150));
                           System.out.println("Answering Banker...");
                           Answer(0);
                           long l3 = System.currentTimeMillis();
                           while(!InBank() && System.currentTimeMillis() - l3 <= Rand(6000,8000))
                               Wait(Rand(100,120));
                        }
                        while(InBank() && CountInv(CollectIDs) > 0)
                        {
                            for(int i = 0; i < CollectIDs.length; i++)
                            {
                                while(InBank() && CountInv(CollectIDs[i]) > 0 && Running())
                                {
                                         Deposit(CollectIDs[i],1);
                                            Wait(Rand(110,152));
                                }
                                Wait(Rand(300,400));
                            }
                            while(InBank() && Running() && CountInv() < 30)
                            {
                                Withdraw(330,1);
                                Wait(Rand(159,200));
                            }
                            if(CountInv(CollectIDs) == 0 && CountInv() == 30)
                                CloseBank();
                        }
                   }
                   long TotalTime = System.currentTimeMillis() - starttime;
                   long Minutes = TotalTime / 60000L;
                   if(Minutes < 1)
                       Minutes = 1;
                   System.out.println("Time Elapsed: " + Minutes + " minutes");
                   System.out.println("Exp Gained: " + (GetExp(1) - exp));
                   System.out.println("Items Collected:");
                   for(int i = 0; i < ItemCounter.length; i++)
                   {
                       if(ItemCounter[i] > 0)
                           System.out.println(ItemName(CollectIDs[i]) + " (" + CollectIDs[i] + "): " + ItemCounter[i]);
                   }
                   while((GetMaxLvl(3) - GetCurLvl(3)) > 12)
                       EatACake();
                   
                   while(GetX() > 327 && Running())
                   {
                       if(GetIdObject(327,552) == 64)
                       {
                           AtObject(327,552);
                           Wait(Rand(800,1200));
                       }
                       WalkTo(326 + Rand(0,2),552 + Rand(0,2));
                       Wait(Rand(800,1200));
                   }
               
                   //walk to gate
                   Walk(325,542);
                   Walk(323,528);
                   Walk(316,519);
                   Walk(325,511);
                   Walk(331,501);
                   Walk(336,491);
                   //go through gate
                   break1 = false;
                   while(!break1)
                   {
                       while(GetX() < 342 && Running())
                       {
                           AtObject(341,487);
                           Wait(Rand(1400,1700));
                       }
                   if(GetX() > 341)
                       break1 = true;
                   }
                   //walk to dungeon
                   Walk(345,494);
                   Walk(352,504);
                   Walk(362,510);
                   Walk(374,515);

                   //go down ladder
                   while(GetY() < 1000)
                   {
                       AtObject(376,520);
                       Wait(Rand(1400,1700));
                   }
               }
           }
        }
        End();
    }

    public void OnChatMessage(String sender, String message)
    {

        if((sender.substring(0,4).equalsIgnoreCase("mod ") || sender.equalsIgnoreCase("andrew") || sender.equalsIgnoreCase("paul")) && Running())
        {
            Display("A mod was detected!");
            Wait(Rand(5000,10000));
            Speak("brb");
            Wait(Rand(2000,5000));
            LogOut();
            Die();
        }
    }

    public void OnPrivateMessage(String sender, String message)
    {

        if((sender.substring(0,4).equalsIgnoreCase("mod ") || sender.equalsIgnoreCase("andrew") || sender.equalsIgnoreCase("paul")) && Running())
        {
            Display("A mod was detected!");
            LogOut();
            Die();
        }
    }

    public void OnServerMessage(String message)
    {
       if((message.substring(0,7).equalsIgnoreCase("your sh")))
       {
           if(!WearingItem(GetItemPos(404))&& fighting)
           {
               WearItem(GetItemPos(404));
               Wait(Rand(800,1200));
           }

       }
    }
}
